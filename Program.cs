﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.Sql;
using Microsoft.Win32;

namespace SQLWithCommandLine
{
    class Program
    { 
        public static void LocateSqlInstances() //List local SQL server instances found from registry. Credits:https://stackoverflow.com/users/364429/isxaker
        {  Console.WriteLine("Method Started...");
            var registryViewArray = new[] { RegistryView.Registry32, RegistryView.Registry64 };
            Console.WriteLine("Accessing Registry...");
            foreach (var registryView in registryViewArray)
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView))
                using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server"))
                {
                    var instances = (string[]) key?.GetValue("InstalledInstances");
                    if (instances != null)
                    {
                        foreach (var element in instances)
                        {   Console.Clear();
                            Console.WriteLine("Printing your Results...");
                            Console.WriteLine();
                            if (element == "MSSQLSERVER")
                                Console.WriteLine("#### "+System.Environment.MachineName+" ####\n");
                            else
                                Console.WriteLine("#### "+System.Environment.MachineName + @"\" + element+" ####\n");

                        }
                    }
                }
            }           
        }


        static void Main(string[] args)
        {   
            string connStr = "Server=DESKTOP-AI8NCQT;Database=Northwind;Trusted_Connection=True;"; //Default
            Console.WriteLine("#### Welcome to SQL Search tool!!! ####\n#### Version 1.0                   ####\n#### Copyright: Sami Kling 2019    ####\n");





            Console.WriteLine("Witch Server are you Connecting to?\n");
            Console.WriteLine("Type 'LIST' to list your local servers.");

            string server =Console.ReadLine();          //Ask user for server input
            if (server.ToUpper() == "LIST")
	        {   
                //Console.WriteLine("Inside of SQL Search...");
                Console.Clear();
                LocateSqlInstances();
                Console.WriteLine("Witch Server are you Connecting to?");
                server = Console.ReadLine();

	        }
            
            Console.WriteLine("Witch Database on Server? Type LIST to list Avaiable Databases."); //Ask user about databases
            string serverDb= Console.ReadLine();
            if (serverDb.ToUpper() == "LIST")
	        {   Console.Clear();
                SqlConnection dbconn = new SqlConnection("Server="+server+";Trusted_Connection=True;");
                dbconn.Open();
                string dblistquery = "SELECT name FROM master.dbo.sysdatabases";
                SqlCommand listdb = new SqlCommand(dblistquery,dbconn);
                SqlDataReader dbreader = listdb.ExecuteReader();
                while (dbreader.Read())
	            {
                    int i=0;
                    string db = dbreader.GetString(i);
                    Console.WriteLine("#### Found DB: "+db+" ####\n");
	            }
                    Console.WriteLine();
                    Console.WriteLine("Witch Database would you like to Connect to?");
                    serverDb = Console.ReadLine();
	        }
            Console.Clear();
            Console.WriteLine("Connecting..."); //Establish connection
            connStr = ("Server="+server+";Database="+serverDb+";Trusted_Connection=True;");
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            Console.WriteLine("Connected.");
            Console.WriteLine("Enter SQL query:");

            //ToDO: Add functionality to ask about avaiable tables and rows in database.

            //string sql = "SELECT * FROM Customers WHERE Country = 'Finland'";
            string sql = Console.ReadLine();
            Console.Clear();
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                
                //int i = 0;
                //string id = reader.GetString(i);
                //Console.WriteLine(id+"\n");
                string companyName = reader.GetString(1);
                string contactName = reader.GetString(2);
                Console.WriteLine("#### Found Line:" + companyName + " # " + contactName+" ####\n");
            }
            Console.ReadLine();
            // resurssien vapautus 
            reader.Close();
            cmd.Dispose();
            conn.Dispose();

        }

    }
}
